$(function(){
    var $HEADER = $('.header');

    var SCROLL_TOP = 0,
        HEADER_HEIGHT = ($HEADER.length) ? Number($HEADER.height()) + Number($HEADER.css('paddingTop').replace('px' , '')) + Number($HEADER.css('paddingBottom').replace('px' , '')) : 0;

    var TAB_DEFAULT_NAME = 'tab__default',
        TAB_HOOK_ID = 'data-tab-id';

    if($HEADER.length){
        $HEADER.after('<div class="strHeader__adjust"></div>');
        var $HEADER_ADJUST = $('.strHeader__adjust').hide().css('height' , HEADER_HEIGHT),
            offSetTop = $HEADER.offset().top;

        $(window).on('scroll', function(){
            SCROLL_TOP = $(this).scrollTop();
            fn_setHeaderFix($HEADER, $HEADER_ADJUST, offSetTop);
        });

        $('.drawer').drawer();
        $('.nav__anchor a').on('click' , function(){
            $('.drawer').drawer('close');
        });
    }



    var fn_setHeaderFix = function($target, $adjust, num){
        if(SCROLL_TOP < num){
            $adjust.hide();
            $target.removeClass('is-fixed');
        }else{
            $adjust.show();
            $target.addClass('is-fixed');
            /*if(animate){
                $target.fadeIn('fast');
            }*/
        }
    }



    /*
     * アコーディオン
     */
    $(function(){
        $(".listDef > .listDef__contents:not(.open)").hide();
        $(".listDef > .listDef__ttl").click(function(){
            $(this).toggleClass("open").next().slideToggle("fast");
        });
    });



    /*
     * ページ先頭へボタン
     */
    $(function() {
        var toPagetop = $('.pagetop');
        toPagetop.hide();
        $(window).scroll(function() {
            if ($(this).scrollTop() > 80) {
                toPagetop.fadeIn();
            } else {
                toPagetop.fadeOut();
            }
        });
    });



    /*
     * 受け取り店舗一覧
     */
    $(function() {
        var $linkBlock = $('.js_areaLinkBlock'),
            $listBlock = $('.js_areaListBlock'),
            param = '?cache='+ Math.random();

        if(($linkBlock.length > 0) || ($listBlock.length > 0)){
            $.getJSON('/common/data/shoplist.json'+ param , function(data) {
                var h2,
                    div, ul, li = '', attend = '',
                    linkClass = 'js_areaLinkBlock__li',
                    tabClass = 'tab',
                    tabTargetCont = '',
                    appendFlag = true,
                    area = '';

                for(var i = 0; i < data.length; i++) {
                    div = '<div class="js_areaListBlock__inner"></div>';
                    h2 = '<h2 class="js_areaListBlock__ttl" id="'+ data[i].id +'">'+ data[i].areaDetail +'</h2>';
                    ul = '<ul class="js_areaListBlock__li"></ul>';

                    if(data[i].region === ''){
                      tabTargetCont = data[i].wide;
                    }else{
                      tabTargetCont = data[i].region;
                    }

                    if($linkBlock.find('['+ TAB_HOOK_ID +'="'+ tabTargetCont +'"]').find('ul.'+ linkClass).length < 1){
                        $linkBlock.find('['+ TAB_HOOK_ID +'="'+ tabTargetCont +'"]').append('<ul class="'+ linkClass +' contents__anchor--type1"></ul>');
                    }

                    if((area === '') || (area !== data[i].area)){
                        $linkBlock.find('['+ TAB_HOOK_ID +'="'+ tabTargetCont +'"] .'+ linkClass).append('<li><a href="#'+ data[i].id +'">'+ data[i].areaDetail +'</a></li>');
                        area = data[i].area;
                    }else{
                        $linkBlock.find('['+ TAB_HOOK_ID +'="'+ tabTargetCont +'"] .'+ linkClass).append('<li><a href="#'+ data[i].id +'">'+ data[i].areaDetail +'</a></li>');
                    }

                    for(var j = 0; j < data[i].list.length; j++){
                        if(data[i].list[j].split(',')[1]){
                            attend = '<span class="attend">'+ data[i].list[j].split(',')[1] +'</span>';
                        }
                        li += '<li>'+ data[i].list[j].split(',')[0] + attend +'</li>';
                        attend = '';
                    }

                    $listBlock.find('['+ TAB_HOOK_ID +'="'+ tabTargetCont +'"]').append(div);
                    $listBlock.find('['+ TAB_HOOK_ID +'="'+ tabTargetCont +'"]').children(':last-child').append(h2);
                    $listBlock.find('['+ TAB_HOOK_ID +'="'+ tabTargetCont +'"]').children(':last-child').append(ul);
                    $listBlock.find('['+ TAB_HOOK_ID +'="'+ tabTargetCont +'"]').find('#'+ data[i].id).next().append(li);

                    li = '';
                }

                fn_SMOOTH_SCROLL();

                if($('.fixed').length){
                    var offset = $('.fixed').offset().top + $('.fixed').height() + Number($('.fixed').css('paddingTop').replace('px' , '')) + Number($('.fixed').css('paddingBottom').replace('px' , ''));
                    fn_FIXED_SCROLL($('.contents__inner--full'), $('.fixed'), offset);
                }
            }).always(function(){
                fn_HASH_SCROLL();
            });
        }else{
            fn_SMOOTH_SCROLL();
        }
    });



    /*
     * お知らせページの補助
     */
    $(function() {
        $('.figureBlock table').each(function(){
            if($(this).find('td').length > 1){
                $(this).addClass('columns');
            }
        });
    });



    /*
     * 画像横幅付与
     */
    $(function() {
        $('.contents img[width]').each(function(){
            $(this).css('width', $(this).attr('width') + 'px');
        });
    });



    /*
     * スライダー横幅付与
     */
    $(function() {
        var w,
            pl, pr,
            space = 15;
        $('.contents .slider__li').each(function(){
            w = $(this).children(0).width();
            pl = Number($(this).children(0).css('paddingLeft').replace('px', ''));
            pr = Number($(this).children(0).css('paddingRight').replace('px', ''));
            $(this).css('width', (w + pl + pr) * $(this).children().length + space + 'px');
        });
    });


    /*
     * タブ
     */
    $(function(){
        var activeIdArr = [];

        $('.tab').each(function(i){
            var $tabContainer = $(this),
                $tabList = $tabContainer.children('.tab__li'),
                tabContWrap = 'tab__contWrap',
                $activeTarget = $tabList.find('li a[id ^= '+ TAB_DEFAULT_NAME +']'),
                activeId = ($activeTarget.length < 1) ? '' : $activeTarget.attr('id');

            if($activeTarget.length < 1){
                $activeTarget = $tabList.find('li:first-child a').attr('id' , TAB_DEFAULT_NAME +  Number(i + 1));
                activeId = TAB_DEFAULT_NAME + Number(i + 1);
            }

            activeIdArr.push($('#'+ activeId).attr('href').replace('#', ''));

            $('.'+ tabContWrap).children('.tab__cont').hide();

            $('['+ TAB_HOOK_ID +'="'+ activeIdArr.join('"], ['+ TAB_HOOK_ID +'="') +'"]').show();

            $($tabList).find('a').click(function(){
                $($tabList).find('a[id ^= '+ TAB_DEFAULT_NAME +']').attr('id' , '');
                $(this).attr('id', activeId);

                activeIdArr = [];

                activeIdArr.push($(this).attr('href').replace('#', ''));
                $('['+ TAB_HOOK_ID +'="'+ $(this).attr('href').replace('#', '') +'"]').find('.tab > .tab__li a[id ^= '+ TAB_DEFAULT_NAME +']').each(function(){
                    activeIdArr.push($(this).attr('href').replace('#', ''));
                });
                $(this).parents('.'+ tabContWrap).parents('.tab').find(' > .tab__li a[id ^= '+ TAB_DEFAULT_NAME +']').each(function(){
                    activeIdArr.push($(this).attr('href').replace('#', ''));
                });

                $('.'+ tabContWrap).children('.tab__cont').hide();
                $('['+ TAB_HOOK_ID +'="'+ activeIdArr.join('"], ['+ TAB_HOOK_ID +'="') +'"]').show();
            })
        });
    });



    /*
     * json読み込み
     */
    if($('.js_jsonInclude').length){
        var $wrap = $('.js_jsonInclude'),
            jsonData, jsonParam,
            paramData,
            recordTags, recordClass = '', recordData, fixHtml = '', locHtml = '',
            linkFlag = false,
            cacheParam = '?cache='+ Math.random();

        if($wrap.is('[data-json]') && $wrap.is('[data-parameter]')){
            jsonData = $wrap.attr('data-json') + cacheParam;
            jsonParam = $wrap.attr('data-parameter').split(',');

            $.getJSON(jsonData , function(data) {
                for(var i = 0; i < data.length; i++){
                    fixHtml += '<li class="jsonInclude__list--val">';

                    for(var j = 0; j < jsonParam.length; j++){
                        jsonParam[j] = jsonParam[j].trim();
                        if(/\./.test(jsonParam[j].split(':')[1])){
                            recordTags = jsonParam[j].split(':')[1].slice(0, jsonParam[j].split(':')[1].indexOf('.'));
                            recordClass = jsonParam[j].split(':')[1].slice(Number(jsonParam[j].split(':')[1].indexOf('.') + 1), jsonParam[j].split(':')[1].length);
                        }else{
                            recordTags = jsonParam[j].split(':')[1];
                        }

                        switch(recordTags){
                            case 'h2':
                                recordClass = (recordClass !== '') ? '<h2 class="'+ recordClass +'">' : '<h2>';
                                locHtml += recordClass + data[i][jsonParam[j].split(':')[0]] + '</h2>';
                            break;

                            case 'h3':
                                recordClass = (recordClass !== '') ? '<h3 class="'+ recordClass +'">' : '<h3>';
                                locHtml += recordClass + data[i][jsonParam[j].split(':')[0]] + '</h3>';
                            break;

                            case 'h4':
                                recordClass = (recordClass !== '') ? '<h4 class="'+ recordClass +'">' : '<h4>';
                                locHtml += recordClass + data[i][jsonParam[j].split(':')[0]] + '</h4>';
                            break;

                            case 'div':
                                recordClass = (recordClass !== '') ? '<div class="'+ recordClass +'">' : '<div>';
                                locHtml += recordClass + data[i][jsonParam[j].split(':')[0]] + '</div>';
                            break;

                            case 'p':
                                recordClass = (recordClass !== '') ? '<p class="'+ recordClass +'">' : '<p>';
                                locHtml += recordClass + data[i][jsonParam[j].split(':')[0]] + '</p>';
                            break;

                            case 'em':
                                recordClass = (recordClass !== '') ? '<em class="'+ recordClass +'">' : '<em>';
                                locHtml += recordClass + data[i][jsonParam[j].split(':')[0]] + '</em>';
                            break;

                            case 'strong':
                                recordClass = (recordClass !== '') ? '<strong class="'+ recordClass +'">' : '<strong>';
                                locHtml += recordClass + data[i][jsonParam[j].split(':')[0]] + '</strong>';
                            break;

                            case 'span':
                                recordClass = (recordClass !== '') ? '<span class="'+ recordClass +'">' : '<span>';
                                locHtml += recordClass + data[i][jsonParam[j].split(':')[0]] + '</span>';
                            break;

                            default:
                                switch(jsonParam[j].split(':')[0]){
                                    case 'link':
                                        fixHtml += (recordTags !== 'blank') ? '<a href="'+ data[i][jsonParam[j].split(':')[0]] +'" class="list-link">' : '<a href="'+ data[i][jsonParam[j].split(':')[0]] +'" class="list-link" target="_blank">';
                                        linkFlag = true;
                                    break;

                                    case 'img':
                                        locHtml += '<img src="'+ data[i][jsonParam[j].split(':')[0]] +'" alt="">';
                                    break;
                                }
                        }
                    }

                    fixHtml += locHtml;
                    fixHtml += (linkFlag) ? '</a></li>' : '</li>';
                    linkFlag = false;
                    locHtml = '';
                }
                $wrap.append('<ul class="jsonInclude__list">'+ fixHtml +'</ul>');
            });
        }
    }



    /*
     * Fixed
     */
    var fn_FIXED_SCROLL = function($container, $target, offSet){
        var $original = $target,
            $clone = $original.clone().addClass('clone'),
            offset = offSet,
            fade = true,
            scroll_top;

        $container.append($clone);
        $clone.hide();

        $(window).on('scroll', function(){
            scroll_top = $(this).scrollTop();

            if(scroll_top > offset){
                if(fade){
                    $clone.fadeIn('fast');
                    fade = false;
                }
            }else{
                if(!fade){
                    $clone.fadeOut('fast');
                   fade = true;
                }
            }
        });
    }



    /*
     * スムーススクロール
     */
    var fn_SMOOTH_SCROLL = function(){
        var scrollTimer = 500;

        $('a[href ^= "#"], .nav__anchor a').on('click' , function(){
            var targetHash = $(this).attr('href').split('#'),
                hashLen = targetHash.length - 1,
                linkPath = targetHash[0].replace('/index.html' , '/'),
                difLink = true,
                sTop = 0;

            if(!$(this).parents().hasClass('not-scroll')){
                if($(this).attr("href") === '#' || $(this).attr("href") === ""){
                    sTop = 0;
                }else{
                    if(linkPath === location.pathname.replace('/index.html' , '/') || linkPath === ''){
                        sTop = $('#'+ targetHash[hashLen]).offset().top - HEADER_HEIGHT;
                    }else{
                        difLink = false;
                        location.href = $(this).attr('href');
                    }
                }

                if(difLink){
                    $('body , html').animate({ scrollTop : sTop } , scrollTimer , 'swing');
                }
            }
            return false;
        });
    }



    /*
     * アンカーリンク調整
     */
    var fn_HASH_SCROLL = function(){
        var hash = location.hash,
            targetPos;

        if(location.hash && $(location.hash).length){
            var tab_hook_id,
                activeId,
                $activeHook,
                activeIdArr = [];

            if($(location.hash).parents('.tab__cont').length){
                tab_hook_id = $(location.hash).parents('.tab__cont').attr(TAB_HOOK_ID);
                $activeHook = $('.tab').find('[href="#'+ tab_hook_id +'"]');
                activeId = $activeHook.parents('.tab__li').find('a[id ^= '+ TAB_DEFAULT_NAME +']').attr('id');

                $activeHook.parents('.tab__li').find('#'+ activeId).attr('id', '');
                $activeHook.attr('id', activeId);

                activeIdArr.push(tab_hook_id);

                $activeHook.parents('.tab__cont').each(function(){
                    tab_hook_id = $(this).attr(TAB_HOOK_ID);
                    $activeHook = $('.tab').find('[href="#'+ tab_hook_id +'"]');
                    activeId = $activeHook.parents('.tab__li').find('a[id ^= '+ TAB_DEFAULT_NAME +']').attr('id');

                    $activeHook.parents('.tab__li').find('#'+ activeId).attr('id', '');
                    $activeHook.attr('id', activeId);

                    activeIdArr.push(tab_hook_id);
                });

                $('.tab__cont').hide();
                $('['+ TAB_HOOK_ID +'="'+ activeIdArr.join('"], ['+ TAB_HOOK_ID +'="') +'"]').show();
            }

            setTimeout(function(){
                HEADER_HEIGHT = ($HEADER.length) ? Number($HEADER.height()) + Number($HEADER.css('paddingTop').replace('px' , '')) + Number($HEADER.css('paddingBottom').replace('px' , '')) : 0;
                targetPos = $(hash).offset().top - HEADER_HEIGHT;
                $('body , html').scrollTop(targetPos);
            }, 500);
        }
    }



    /*
     * スクロール調整
     */
    $(window).on('load', function(){
        fn_HASH_SCROLL();

    /*
     * Slickスライダー
     */
        if($('.slickSlider').length > 0){
            $('.slickSlider').slick({
                adaptiveHeight : true,
                centerMode : true,
                dots: true,
                centerPadding:'20px'
            });
        }

    /*
     * 高さ揃え
     */
        if($('.contents__img--type4 .item').length > 0){
            $('.contents__img--type4 .item').matchHeight();
        }
        if($('.figureBlock.itemList .caption').length > 0){
            $('.figureBlock.itemList .caption').matchHeight();
        }
    });


    /* Lazy load */
    if($('img.lazyload').length){
        $("img.lazyload").lazyload({
            skip_invisible: true
        });
    }
});
